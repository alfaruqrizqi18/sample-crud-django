from django import forms

from .models import *

class FormCreateBiodata(forms.ModelForm):
    class Meta:
        model = Siswa
        fields = ('nama_siswa', 'kelamin', 'kelas', 'alamat',)

class FormUpdateBiodata(forms.ModelForm):
    class Meta:
        model = Siswa
        fields = ('nama_siswa', 'kelamin', 'kelas', 'alamat',)
