from django.db import models

# Create your models here.

class Kelas(models.Model):
    nama_kelas  = models.CharField(max_length=30, blank=False)
    status      = models.CharField(max_length=10, default="Aktif")

    def __unicode__(self):
        return self.nama_kelas

    def __str__(self):
        return self.nama_kelas

class Siswa(models.Model):
    nama_siswa  = models.CharField(max_length=100, blank=False)
    kelamin     = models.CharField(max_length=2, blank=False)
    kelas       = models.ForeignKey(Kelas)
    alamat      = models.TextField(blank=True)

    def __unicode__(self):
        return self.nama_siswa

    def __str__(self):
        return self.nama_siswa
