from django.conf.urls import url
from . import views

urlpatterns = [
    #nama function harus sama dengan function di views
    url(r'^beranda/$', views.Beranda, name='Beranda'),
    url(r'^create/$', views.Create, name='Create'),
    url(r'^delete/(?P<pk>\d+)/$', views.Delete, name='Delete'),
    url(r'^update/(?P<pk>\d+)/$', views.Update, name='Update'),
]
