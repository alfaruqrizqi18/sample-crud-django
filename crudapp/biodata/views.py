from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *
# Create your views here.

def Beranda(request):
    query = Siswa.objects.all().order_by('-pk') #ORDER BY PRIMARY KEY DESC
    return render(request, 'beranda.html', {'datasiswa':query})

def Create(request):
    if request.method == "POST":
        form = FormCreateBiodata(request.POST)
        if form.is_valid():
            biodata = form.save(commit=False)
            biodata.save()
            return redirect('Beranda')
    else:
        query = Kelas.objects.all()
        return render(request, 'create.html', {'datakelas':query})

def Delete(request, pk):
    query = get_object_or_404(Siswa, pk = pk)
    query.delete()
    return redirect('Beranda')

def Update(request, pk):
    query = get_object_or_404(Siswa, pk=pk)
    if request.method == "POST":
        form = FormUpdateBiodata(request.POST, instance=query)
        if form.is_valid():
            query = form.save(commit=False)
            query.save()
            return redirect('Beranda')
    else:
        kelas = Kelas.objects.all()
        query = get_object_or_404(Siswa, pk=pk)
        return render(request, 'update.html', {'datasiswa':query, 'datakelas':kelas})
