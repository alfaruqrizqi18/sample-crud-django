# Sample Crud Django

Untuk belajar bareng.

# How to run these project ?

1. Download project django ini
2. Ubah nama folder menjadi django (opsional : supaya lebih mudah di akses)
3. cd django
4. Aktifkan Virtual Environtment -> ketik di terminal : source env/bin/activate
5. Setelah aktif, masuk ke direktori crudapp -> cd crudapp
6. Setelahnya, run project ini -> python manage.py runserver
7. Buka browser, localhost:8000/biodata/beranda
8. Oke, this project running.

# Finish
Muhammad Rizqi Alfaruq - Best Regards